﻿using System.Collections.Generic;

namespace TroyCMS.DataAccess.Models
{
    public class GetCommentDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public int Order { get; set; }

        public ICollection<GetCommentDTO> Replies { get; set; }
    }
}