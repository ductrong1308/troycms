﻿using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Repositories
{
    public class ImageRepository : RepositoryBase<Image>, IImageRepository
    {
        public ImageRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IImageRepository : IRepository<Image>
    {
    }
}