﻿using AutoMapper;
using TroyCMS.Entities.Models;
using TroyCMS.DataAccess.Models;

namespace TroyCMS.DataAccess.Mappers
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<EFOToDTOMappingProfile>();
                x.AddProfile<DTOToEFOMappingProfile>();
            });
        }

        public class DTOToEFOMappingProfile : Profile
        {
            public override string ProfileName
            {
                get { return "DTOToEFOMappings"; }
            }

            protected override void Configure()
            {
                Mapper.CreateMap<CreateUserDTO, User>();
                Mapper.CreateMap<NewPostDTO, Post>();
            }
        }

        public class EFOToDTOMappingProfile : Profile
        {
            public override string ProfileName
            {
                get { return "EFOToDTOMappings"; }
            }

            protected override void Configure()
            {
                Mapper.CreateMap<User, GetUserDTO>();
                Mapper.CreateMap<Post, GetGagDTO>();
                Mapper.CreateMap<Comment, GetCommentDTO>();
            }
        }
    }
}