namespace TroyCMS.Entities.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name  { get; set; } 
        public string Description { get; set; }
        public string Avatar { get; set; }
        public TagType TagType { get; set; }
        public int TagTypeId { get; set; }
        public int SearchCount { get; set; }
        public short Level { get; set; }

        public int PostId { get; set; }
        public virtual Post Post { get; set; }
    }
}