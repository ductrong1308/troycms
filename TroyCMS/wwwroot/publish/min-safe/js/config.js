var baseUrl = $('base').attr('href');
var appBaseUrl = baseUrl + 'app/';
var hostName = baseUrl.replace('wwwroot/', '');

troyCMS
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise("/pages/wall");


        $stateProvider
        
            //------------------------------
            // PAGES
            //------------------------------
            
            .state ('pages', {
                url: '/pages',
                templateUrl: appBaseUrl + 'pages/common/views/common.html'
            })
            
            .state ('pages.wall', {
                url: '/wall',
                templateUrl: appBaseUrl + 'pages/wall/views/wall.html',
                resolve: {
                    loadPlugin: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'vendors',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/autosize/dist/autosize.min.js'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/mediaelement/build/mediaelement-and-player.min.js',
                                    'vendors/bower_components/lightgallery/dist/js/lg-thumbnail.min.js',
                                    'vendors/bower_components/angular-file-upload/dist/angular-file-upload.min.js',
                                    'vendors/bower_components/jquery.gifplayer/jquery.gifplayer.min.js',
                                    'vendors/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.min.js',
                                    'vendors/bower_components/jquery_scrollIntoView/jquery.scrolling.min.js',
                                    'vendors/fileinput/fileinput.min.js'
                                ]
                            }
                        ])
                    }]
                }
            })
    }]);
