﻿using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TroyCMS.DataAccess;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.DataAccess.Infrastructure.Identity;
using TroyCMS.Entities.Models;
using TroyCMS.DataAccess.Models;
using TroyCMS.DataAccess.Repositories;

namespace TroyCMS.DataAccess.Services
{
    public class PostService : IPostService
    {
        private readonly IPostRepository postRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly TroyCMSIdentityContext _ictx;
        private readonly TroyCMSUserManager _userManager;

        public PostService(IPostRepository postRepository, IUnitOfWork unitOfWork)
        {
            this.postRepository = postRepository;
            this.unitOfWork = unitOfWork;
            _ictx = new TroyCMSIdentityContext();
            _userManager = new TroyCMSUserManager(new UserStore<User>(_ictx));
        }

        public async Task<IEnumerable<GetGagDTO>> GetGags(int skip, int take)
        {
            var gags = postRepository.GetMany(p => p.PostType == PostType.GAG).OrderByDescending(p => p.PublishedDate).Skip(skip).Take(take).Select(Mapper.Map<GetGagDTO>).ToList();
            foreach(var gag in gags)
            {
                var user = await _userManager.FindByIdAsync(gag.CreatedUserId.ToLower());
                gag.CreatedUserName = user.FirstName + " " + user.LastName;
            }
            return gags;
        }

        public async Task<bool> CreatePost(NewPostDTO newPost)
        {
            var user = await _userManager.FindByNameAsync("TroyCMSSA");
            var post = Mapper.Map<Post>(newPost);
            post.CreatedUser = user;
            post.CreatedUserId = user.Id;
            post.CreatedDate = DateTime.Now;
            post.UpdatedUser = user;
            post.UpdatedUserId = user.Id;
            post.UpdatedDate = DateTime.Now;
            post.PublishedDate = DateTime.Now;
            foreach (var image in post.Images)
            {
                image.CreatedUser = user;
                image.CreatedUserId = user.Id;
                image.CreatedDate = DateTime.Now;
                image.UpdatedUser = user;
                image.UpdatedUserId = user.Id;
                image.UpdatedDate = DateTime.Now;
            }

            postRepository.Add(post);
            unitOfWork.Commit();
            return true;
        }
    }

    public interface IPostService
    {
        Task <IEnumerable<GetGagDTO>> GetGags(int skip, int take);
        Task<bool> CreatePost(NewPostDTO newPost);
    }
}