﻿using System;
using System.Collections.Generic;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Models
{
    public class GetGagDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime? PublishedDate { get; set; }
        public string CreatedUserId { get; set; }
        public string CreatedUserName { get; set; }
        
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<GetCommentDTO> Comments { get; set; }
    }
}