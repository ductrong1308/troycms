namespace TroyCMS.DataAccess.Migrations.Identity
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Entities.Models;
    using Microsoft.AspNet.Identity;

    internal sealed class Configuration : DbMigrationsConfiguration<TroyCMS.DataAccess.TroyCMSIdentityContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"Migrations\Identity";
        }

        protected override void Seed(TroyCMS.DataAccess.TroyCMSIdentityContext context)
        {
            //  This method will be called after migrating to the latest version.

            var manager = new UserManager<User>(new UserStore<User>(new TroyCMSIdentityContext()));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new TroyCMSIdentityContext()));

            var user = new User()
            {
                UserName = "TroyCMSSA",
                Email = "troyuit@gmail.com",
                EmailConfirmed = true,
                FirstName = "Troy",
                LastName = "Lee",
                Level = 1,
                JoinedDate = DateTime.Now.AddYears(-3)
            };

            manager.Create(user, "Asdcxz1+");

            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new IdentityRole { Name = "SuperAdmin" });
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "User" });
            }

            var adminUser = manager.FindByName("TroyCMSSA");

            manager.AddToRoles(adminUser.Id, new string[] { "SuperAdmin", "Admin" });
        }
    }
}
