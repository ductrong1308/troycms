﻿namespace TroyCMS.Entities.Models
{
    public enum ImageType
    {
        Static, Gif
    }
}
