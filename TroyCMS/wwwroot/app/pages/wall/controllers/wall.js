﻿troyCMS
    // =========================================================================
    // Wall
    // =========================================================================
    .controller('wallCtrl', ['$sce', '$scope', '$timeout', '$http', 'FileUploader', 'Gag', function ($sce, $scope, $timeout, $http, FileUploader, Gag) {
        var _self = this;

        _self.wallCommenting = [];
        _self.wallCommentContent = [];

        _self.setDefaultStatus;
        _self.acceptFileFormats = "";

        _self.trustAsResourceUrl = $sce.trustAsResourceUrl;

        _self.Gag = new Gag();

        _self.uploader = new FileUploader({
            url: hostName + 'api/upload'
        });

        _self.uploader.filters.push({
            name: 'extensionFilter',
            fn: function (item, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                if ('|jpg|png|jpeg|bmp|gif|mp4|'.indexOf(type) != -1) {
                    return true;
                }
                swal("Invalid file format!", 'Please select a file with jpg, png, jpeg, bmp, gif or mp4 format and try again.', "error");
                return false;
            }
        }, {
            name: 'sizeFilter',
            fn: function (item, options) {
                var fileSize = parseInt(item.size);
                if (fileSize <= 5242880) {
                    return true;
                }
                swal("Selected file exceeds the 5MB file size limit!", 'Please choose a new file and try again.', "error");
                return false;
            }
        });

        _self.uploader.onAfterAddingAll = function () {
            if (_self.uploader.queue.length == 1) {
                var file = _self.uploader.queue[0]._file;
                var type = file.type;
                if (type === "video/mp4") {
                    file.fileURL = URL.createObjectURL(file);
                }
            }
        }

        _self.selectFiles = function () {
            var imageType = 1;
            _self.wallImage = true;
            _self.changeUploaderAcceptedFileFormat(imageType);
            _self.uploadNewFiles();
        }

        _self.selectGif = function () {
            var gifType = 2;
            _self.wallGif = true;
            _self.changeUploaderAcceptedFileFormat(gifType);
            _self.uploadNewFiles();
        }

        _self.changeUploaderAcceptedFileFormat = function (uploaderType) {
            var uploader = $("#uploader")[0];
            uploader.removeAttribute('accept');
            switch (uploaderType) {
                case 1: // Images
                    $(uploader).attr('accept', 'image/jpg, image/png, image/jpeg, img/bmp');
                    $(uploader).attr('multiple', '');
                    break;
                case 2: // Gif, MP4
                    $(uploader).attr('accept', 'image/gif, video/mp4');
                    uploader.removeAttribute('multiple');
                    break;
                default:
                    break;
            }
        }

        _self.setDefaultStatus = function () {
            _self.wallImage = false;
            _self.wallGif = false;
            _self.wallVideo = false;
            _self.wallLink = false;
            _self.wallPostingVisible = false;
        }

        _self.uploadNewFiles = function () {
            if (_self.uploader.queue.length > 0) {
                swal({
                    title: "Are you sure?",
                    text: "Do you want to upload the current selected files?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#DB4437",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        _self.post();
                    } else {
                        _self.uploader.clearQueue();
                        $timeout(function () {
                            $("#btnWallPosting").click();
                            $('#uploader').click();
                        }, 10);
                    }
                });
            } else {
                $timeout(function () {
                    $('#uploader').click();
                }, 10);
            }
        }

        _self.clear = function () {
            swal({
                title: "Are you sure?",
                text: "Do you want clear all uploaded file?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#DB4437",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    _self.uploader.clearQueue();
                }

                $timeout(function () {
                    $("#btnWallPosting").click();
                }, 10);
            });
        }

        _self.newPost = {
            PostType: 'GAG',
            Images: [],
            Title: '',
            Description: ''
        };

        _self.post = function () {
            _self.uploader.uploadAll();
            _self.uploader.onSuccessItem = function (item, response) {
                _self.newPost.Images.push({
                    ImageType: 'Static',
                    Url: response.url
                });
            }
            _self.uploader.onCompleteAll = function () {
                if (_self.newPost.Images.length > 0) {
                    $http({
                        url: hostName + 'api/post/create',
                        method: "POST",
                        data: _self.newPost
                    }).success(function (result) {
                        swal("Đăng thành công!", result, "success");
                        _self.uploader.clearQueue();
                        _self.setDefaultStatus();

                        _self.newPost = {
                            PostType: 'GAG',
                            Images: [],
                            Title: '',
                            Description: ''
                        };
                    }).error(function (result, status) {
                        swal("Err!", result, "error");
                    });
                }
            }
        }

        _self.postToFeed = function (gag) {
            FB.ui({
                method: 'feed',
                link: 'http://bongvl.vn',
                caption: gag.Description,
            }, function (response) { });
        }
    }])

