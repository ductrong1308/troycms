﻿namespace TroyCMS.DataAccess.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
