﻿using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Repositories
{
    public class PostRepository : RepositoryBase<Post>, IPostRepository
    {
        public PostRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IPostRepository : IRepository<Post>
    {
    }
}