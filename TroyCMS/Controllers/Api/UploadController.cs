﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace TroyCMS.ApiControllers
{
    public class UploadController : BaseApiController
    {
        private Cloudinary cloudinary = null;

        public UploadController()
        {
            Account account = new Account(
                  "doua5pgdi",
                  "696442329793211",
                  "7eVrRtWC2xJ68tiNIAmBPb93bOU");
            cloudinary = new Cloudinary(account);
        }

        [HttpPost]
        [Route("api/upload")]
        public RawUploadResult Upload()
        {
            RawUploadResult result = null;

            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["file"];

                if(httpPostedFile.ContentType == "video/mp4")
                {
                    var videoUploadParams = new VideoUploadParams()
                    {
                        File = new FileDescription(httpPostedFile.FileName, httpPostedFile.InputStream)
                    };

                    result = cloudinary.Upload(videoUploadParams);
                }
                else
                {
                    var imageUploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(httpPostedFile.FileName, httpPostedFile.InputStream)
                    };

                    result = cloudinary.Upload(imageUploadParams);
                }
            }

            return result;
        }
    }
}