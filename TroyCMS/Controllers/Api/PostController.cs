﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using TroyCMS.DataAccess.Models;
using TroyCMS.DataAccess.Repositories;
using TroyCMS.DataAccess.Services;

namespace TroyCMS.ApiControllers
{
    public class PostController : ApiController
    {
        private readonly IPostService postService;

        public PostController(IPostService postService)
        {
            this.postService = postService;
        }
        
        [HttpPost]
        [Route("api/post/create")]
        public async Task<string> CreatePost(NewPostDTO newPost)
        {
            if (!string.IsNullOrEmpty(newPost.Description) && !newPost.Description.Contains("#TroyCMSSA"))
            {
                return "Xin lỗi chỉ có BQT được đăng bài tại thời điểm này";
            }
            newPost.Description.Replace("#TroyCMSSA", "");
            await postService.CreatePost(newPost);
            return "Cảm ơn bạn đã đóng góp cho BONGVL.VN. Chúng tôi sẽ kiểm duyệt và công bố Gag này trong thời gian sớm nhất."; 
        }

        [Route("api/post/getgags")]
        public async Task<IEnumerable<GetGagDTO>> GetGags(int skip, int take)
        {
            return await postService.GetGags(skip, take);
        }
    }
}
