﻿using System;

namespace TroyCMS.DataAccess.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        TroyCMSContext Init();
    }
}
