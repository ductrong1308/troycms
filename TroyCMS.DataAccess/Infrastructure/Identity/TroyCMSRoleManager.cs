﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace TroyCMS.DataAccess.Infrastructure.Identity
{
    public class TroyCMSRoleManager : RoleManager<IdentityRole>
    {
        public TroyCMSRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static TroyCMSRoleManager Create(IdentityFactoryOptions<TroyCMSRoleManager> options, IOwinContext context)
        {
            var troyCMSRoleManager = new TroyCMSRoleManager(new RoleStore<IdentityRole>(context.Get<TroyCMSIdentityContext>()));

            return troyCMSRoleManager;
        }
    }
}
