﻿troyCMS
    .filter('moment', function () {
        return function (date) {
            moment.locale('vi');
            return moment(date).fromNow();
        };
    });