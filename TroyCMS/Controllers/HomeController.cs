﻿using System.Configuration;
using System.Web.Mvc;

namespace TroyCMS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.AppName = ConfigurationManager.AppSettings["AppName"];
            return View();
        }
    }
}
