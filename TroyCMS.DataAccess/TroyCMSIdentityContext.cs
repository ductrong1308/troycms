﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess
{
    public class TroyCMSIdentityContext : IdentityDbContext<User>
    {
        public TroyCMSIdentityContext()
            : base("TroyCMSContext", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public static TroyCMSIdentityContext Create()
        {
            return new TroyCMSIdentityContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // This needs to go before the other rules!

            modelBuilder.Entity<User>().ToTable("Users", "dbo").HasKey(p => p.Id);
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles", "dbo");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins", "dbo");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims", "dbo").HasKey(p => p.Id);
            modelBuilder.Entity<IdentityRole>().ToTable("Roles", "dbo").HasKey(p => p.Id);
            modelBuilder.Ignore<IdentityUser>();
            modelBuilder.Ignore<Player>();
            modelBuilder.Ignore<Team>();
        }
    }
}
