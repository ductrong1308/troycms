troyCMS
    // =========================================================================
    // Base controller for common functions
    // =========================================================================

    .controller('troyCMSCtrl', ['$timeout', '$state', 'growlService', function($timeout, $state, growlService){
        //Welcome Message
        growlService.growl('Welcome back Troy Lee!', 'inverse')
        
        
        // Detact Mobile Browser
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        }

        // By default template has a boxed layout
        this.layoutType = localStorage.getItem('ma-layout-status');
        
        // For Mainmenu Active Class
        this.$state = $state;    
        
        //Close sidebar on click
        this.sidebarStat = function(event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                this.sidebarToggle.left = false;
            }
        }
        
        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;
        
        this.lvSearch = function() {
            this.listviewSearchStat = true; 
        }
        
        //Listview menu toggle in small screens
        this.lvMenuStat = false;
        
    }])

    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', ['$timeout', 'messageService', function($timeout, messageService){
    
         // Top Search
        this.openSearch = function(){
            angular.element('#header').addClass('search-toggled');
            //growlService.growl('Welcome back Mallinda Hollaway', 'inverse');
        }

        this.closeSearch = function(){
            angular.element('#header').removeClass('search-toggled');
        }
        
        // Get messages and notification for header
        this.img = messageService.img;
        this.user = messageService.user;
        this.user = messageService.text;

        this.messageResult = messageService.getMessage(this.img, this.user, this.text);


        //Clear Notification
        this.clearNotification = function($event) {
            $event.preventDefault();
            
            var x = angular.element($event.target).closest('.listview');
            var y = x.find('.lv-item');
            var z = y.size();
            
            angular.element($event.target).parent().fadeOut();
            
            x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
            x.find('.grid-loading').fadeIn(1500);
            var w = 0;
            
            y.each(function(){
                var z = $(this);
                $timeout(function(){
                    z.addClass('animated fadeOutRightBig').delay(1000).queue(function(){
                        z.remove();
                    });
                }, w+=150);
            })
            
            $timeout(function(){
                angular.element('#notifications').addClass('empty');
            }, (z*150)+200);
        }
        
        // Clear Local Storage
        this.clearLocalStorage = function() {
            
            //Get confirmation, if confirmed clear the localStorage
            swal({   
                title: "Are you sure?",   
                text: "All your saved localStorage values will be removed",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DB4437",
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
                localStorage.clear();
                swal("Done!", "localStorage is cleared", "success"); 
            });
            
        }
        
        //Fullscreen View
        this.fullScreen = function() {
            //Launch
            function launchIntoFullscreen(element) {
                if(element.requestFullscreen) {
                    element.requestFullscreen();
                } else if(element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if(element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if(element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            //Exit
            function exitFullscreen() {
                if(document.exitFullscreen) {
                    document.exitFullscreen();
                } else if(document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if(document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            }
            else {
                launchIntoFullscreen(document.documentElement);
            }
        }
    
    }])



troyCMS
    // =========================================================================
    // Wall
    // =========================================================================
    .controller('wallCtrl', ['$timeout', '$http', 'FileUploader', 'Gag', function ($timeout, $http, FileUploader, Gag) {
        var _self = this;

        _self.Gag = new Gag();

        _self.wallCommenting = [];
        _self.wallCommentContent = [];

        _self.wallImage = false;
        _self.wallVideo = false;
        _self.wallLink = false;

        _self.uploader = new FileUploader({
            url: hostName + 'api/upload'
        });

        _self.uploader.filters.push({
            name: 'extensionFilter',
            fn: function (item, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                if ('|jpg|png|jpeg|bmp|gif|mp4|'.indexOf(type) != -1) {
                    return true;
                }
                swal("Invalid file format!", 'Please select a file with jpg, png, jpeg, bmp, gif or mp4 format and try again.', "error");
                return false;
            }
        }, {
            name: 'sizeFilter',
            fn: function (item, options) {
                var fileSize = parseInt(item.size);
                if (fileSize <= 5242880) {
                    return true;
                }
                swal("Selected file exceeds the 5MB file size limit!", 'Please choose a new file and try again.', "error");
                return false;
            }
        });

        _self.uploader.onAfterAddingAll = function () {
            _self.wallImage = true;
        }

        _self.selectFiles = function () {
            $('#uploader').click();
        }

        _self.newPost = {
            PostType: 'GAG',
            Images: []
        };

        _self.post = function () {
            _self.uploader.uploadAll();
            _self.uploader.onSuccessItem = function (item, response) {
                _self.newPost.Images.push({
                    ImageType: 'Static',
                    Url: response.url
                });
            }
            _self.uploader.onCompleteAll = function () {
                if (_self.newPost.Images.length > 0) {
                    $http({
                        url: hostName + 'api/post/create',
                        method: "POST",
                        data: _self.newPost
                    }).success(function (result) {
                        swal("Đăng thành công!", result, "success");
                        _self.uploader.clearQueue();
                        _self.wallImage = false;
                    }).error(function (result, status) {
                        swal("Err!", result, "error");
                    });
                }
            }
        }

        _self.postToFeed = function (gag) {
            FB.ui({
                method: 'feed',
                link: 'http://bongvl.vn',
                caption: gag.Description,
            }, function (response) { });
        }
    }])

