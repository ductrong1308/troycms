﻿using System;
using System.Collections.Generic;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Models
{
    public class NewPostDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Avatar { get; set; }
        public PostType PostType { get; set; }
        
        public virtual ICollection<Image> Images { get; set; }
    }
}