troyCMS
    .factory('Gag', ['$http', function ($http) {
        var Gag = function () {
            this.items = [];
            this.busy = false;
            this.skip = 0;
            this.take = 5;
        };

        Gag.prototype.nextPage = function () {
            var _self = this;
            if (_self.busy) return;
            _self.busy = true;

            var url = hostName + 'api/post/getgags';
            $http.get(url, {
                params: {
                    skip: _self.skip,
                    take: _self.take
                }
            }).success(function (data) {
                var items = data;
                if (items.length == 0) {
                    swal("Warning!", 'No more data to load!', "warning");
                    return;
                }
                items.forEach(function (item) {
                    _self.items.push(item);
                });
                _self.busy = false;
                _self.skip += _self.take;
            });
        };

        return Gag;
    }]);